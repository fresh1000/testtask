<html>
<head>
    <meta charset="UTF-8">
    <title>Products</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
    <body>

    <div class="container">
        <form action="{{route('ProductsShow')}}" method="GET" class="form-horizontal" enctype="multipart/form-data">

            <div class="form-group">
                {{ Form::label('selectSort', 'Sort By:') }}
                {{ Form::select('selectSort', ['name' => 'Name', 'price' => 'Price', 'year' => 'Year'], Input::get('selectSort', 'name'), ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('order', 'Order:') }}
                {{ Form::select('order', ['asc' => 'ASC', 'desc' => 'DESC'], Input::get('order', 'asc'), ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('paginate', 'Paginate:') }}
                {{ Form::select('paginate', ['10' => '10', '20' => '20', '50' => '50'], Input::get('paginate', '20'), ['class' => 'form-control']) }}
            </div>

            <button type="submit" class="btn btn-primary btn-md">Sort</button>
        </form>

        <ul class="pagination">{{$products->appends($forPaginate)->links()}}</ul>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Created_at</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->year}}</td>
                            <td>{{$product->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        <ul class="pagination">{{$products->appends($forPaginate)->links()}}</ul>

    </div>
    </body>
</html>