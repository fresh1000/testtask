<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Obyava</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header>

		<div class="header__top">
			<div class="wrapper">
				<div class="header__logo">
					<img src="img/header_logo.png" alt="">
				</div>

				<div class="header__profile">
					<span class="profile__image"><img src="img/profile_image.png" alt=""></span>
					<span class="profile__title">Профиль</span>
					<span class="profile__icon"><img src="img/profile_icon.png" alt=""></span>
				</div>

				<div class="header__search">
					<form action="">
						<!-- <span class="search__icon"><img src="img/search_icon.png" alt=""></span> -->
						<input class="search__input" type="text" name="" placeholder="Поиск">
						<input class="search__region" type="text" name="" placeholder="Регион">
						<input class="search__button" type="submit" value="" name="" >
					</form>
				</div>
				<div class="clear">
				</div>
			</div>
		</div>

		<div class="header__bottom">
			<div class="wrapper">
				<h1 class="header__title">Портал объявлений</h1>
				<a class="header__btn" href="#"><!-- <img class="btn__icon" src="img/btn_icon.png" alt=""> -->Разместить объявление</a>
			</div>
		</div>

	</header>
	<main>

		<div class="wrapper">
			<div class="content">
				<div class="category">

					<div class="category__item">
						<div class="category__img"><img src="img/category_1.png" alt=""></div>
						<div class="category__name">Недвижимость</div>
						<div class="category__count">(7423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_2.png" alt=""></div>
						<div class="category__name">Электроника</div>
						<div class="category__count">(2212)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_3.png" alt=""></div>
						<div class="category__name">Транспорт</div>
						<div class="category__count">(4234)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_4.png" alt=""></div>
						<div class="category__name">Работа</div>
						<div class="category__count">(9821)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_5.png" alt=""></div>
						<div class="category__name">Мебель, посуда</div>
						<div class="category__count">(2422)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_2.png" alt=""></div>
						<div class="category__name">Одежда, аксессуары</div>
						<div class="category__count">(1423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_3.png" alt=""></div>
						<div class="category__name">Дом, сад</div>
						<div class="category__count">(7423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_1.png" alt=""></div>
						<div class="category__name">Строительство, ремонт</div>
						<div class="category__count">(10423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_5.png" alt=""></div>
						<div class="category__name">Детские товары</div>
						<div class="category__count">(4423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_4.png" alt=""></div>
						<div class="category__name">Животные, растения</div>
						<div class="category__count">(6223)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_1.png" alt=""></div>
						<div class="category__name">Здоровье, красота</div>
						<div class="category__count">(1423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_2.png" alt=""></div>
						<div class="category__name">Хобби, спорт</div>
						<div class="category__count">(5423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_3.png" alt=""></div>
						<div class="category__name">Бизнес услуги</div>
						<div class="category__count">(2123)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_4.png" alt=""></div>
						<div class="category__name">Продукты питания</div>
						<div class="category__count">(6423)</div>
					</div>

					<div class="category__item">
						<div class="category__img"><img src="img/category_5.png" alt=""></div>
						<div class="category__name">Знакомства</div>
						<div class="category__count">(2423)</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>

	</main>
	<footer>

		<div class="footer__top">
			<div class="wrapper">
				<div class="footer__title">Хотите продать?</div>

				<div class="footer__btn">
					<a class="btn__icon__footer" href="#">Разместить объявление</a>
				</div>
				<div class="footer__info">В интернете есть много площадок для размещения объявлений. Но до сих пор не было такой удобной. Инновации, удобство и простота - это то, чем отличается портал Obyava.ua. </div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="wrapper">
				<div class="footer__logo">
					<img src="img/footer_logo.png" alt="">
				</div>
				<nav class="footer__menu">
					<li class="menu__item"><a class="menu__link" href="#">About us</a></li>
					<li class="menu__item"><a class="menu__link"  href="#">Mobile website</a></li>
					<li class="menu__item"><a class="menu__link"  href="#">FAQ</a></li>
					<li class="menu__item"><a class="menu__link"  href="#">Paid servicies</a></li>
					<li class="menu__item"><a class="menu__link"  href="#">Terms and conditions</a></li>
					<li class="menu__item"><a class="menu__link"  href="#">Help</a></li>
					<div class="clear"></div>
				</nav>
			</div>
		</div>
	</footer>
</body>
</html>