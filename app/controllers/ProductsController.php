<?php

class ProductsController extends BaseController
{


    public function show()
    {
        $paginate = 20;
        $sortby = 'name';
        $order = 'asc';

        $forPaginate = array();
        if(Input::has('paginate') && Input::get('paginate') != $paginate) {
            $paginate = Input::get('paginate');
            $forPaginate['paginate'] = $paginate;
        }
        if(Input::has('selectSort') && Input::get('selectSort') !== $sortby) {
            $sortby = Input::get('selectSort');
            $forPaginate['selectSort'] = $sortby;
        }
        if(Input::has('order') && Input::get('order') !== $order) {
            $order = Input::get('order');
            $forPaginate['order'] = $order;
        }

        $products = DB::table('products')->orderBy($sortby, $order)->paginate($paginate);

        return View::make('products', compact('products', 'forPaginate'));
    }
}