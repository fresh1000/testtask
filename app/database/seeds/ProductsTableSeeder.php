<?php

class ProductsTableSeeder  extends Seeder {

    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++)
        {
            DB::table('products')->insert([
                'name' => $faker->name,
                'price' => $faker->numberBetween(1, 1000000),
                'description' => $faker->text(),
                'year' => $faker->year,
                'created_at' => $faker->dateTime,
            ]);
        }



    }

}